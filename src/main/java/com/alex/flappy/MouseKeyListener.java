package com.alex.flappy;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class MouseKeyListener implements MouseListener, KeyListener {

    private final GameController flappyBirdGameController;

    public MouseKeyListener(GameController flappyBirdGameController) {
        this.flappyBirdGameController = flappyBirdGameController;
    }

    public void mouseClicked(MouseEvent e) {
        flappyBirdGameController.jump();
    }


    public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_SPACE) {
            flappyBirdGameController.jump();
        }
    }


    public void mousePressed(MouseEvent e) {
    }


    public void mouseReleased(MouseEvent e) {
    }


    public void mouseEntered(MouseEvent e) {
    }


    public void mouseExited(MouseEvent e) {
    }


    public void keyTyped(KeyEvent e) {
    }

    public void keyPressed(KeyEvent e) {
    }
}
