package com.alex.flappy;

import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import static com.alex.flappy.FlappyBirdGame.HEIGHT;

public class TimerActionListener implements ActionListener {

    private final GameController gameController;
    private int ticks;
    private final FlappyBirdGame fpGame;
    private final Renderer renderer;

    public TimerActionListener(FlappyBirdGame fpGame, Renderer renderer, GameController gameController) {
        this.fpGame = fpGame;
        this.renderer = renderer;
        this.gameController = gameController;
        ticks = 0;
    }


    public void actionPerformed(ActionEvent e) {
        int speed = 10;

        ticks++;

        if (fpGame.isStarted()) {
            final List<Rectangle> columns = fpGame.getColumns();
            for (Rectangle column : columns) {
                column.x -= speed;
            }

            if (ticks % 2 == 0 && fpGame.getYMotion() < 15) {
                fpGame.incYMotion(2);
            }

            for (int i = 0; i < columns.size(); i++) {
                Rectangle column = columns.get(i);

                if (column.x + column.width < 0) {
                    columns.remove(column);

                    if (column.y == 0) {
                        gameController.addColumn(false);
                    }
                }
            }

            final Rectangle bird = fpGame.getBird();
            bird.y += fpGame.getYMotion();

            for (Rectangle column : columns) {
                if (column.y == 0 && bird.x + bird.width / 2 > column.x + column.width / 2 - 10 && bird.x + bird.width / 2 < column.x + column.width / 2 + 10) {
                    fpGame.incScore();
                }

                if (column.intersects(bird)) {
                    fpGame.gameOver();

                    if (bird.x <= column.x) {
                        bird.x = column.x - bird.width;

                    } else {
                        if (column.y != 0) {
                            bird.y = column.y - bird.height;
                        } else if (bird.y < column.height) {
                            bird.y = column.height;
                        }
                    }
                }
            }

            if (bird.y > HEIGHT - 120 || bird.y < 0) {
                fpGame.gameOver();
            }

            if (bird.y + fpGame.getYMotion() >= HEIGHT - 120) {
                bird.y = HEIGHT - 120 - bird.height;
                fpGame.gameOver();
            }
        }

        renderer.repaint();
    }

}
