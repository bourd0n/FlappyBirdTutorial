package com.alex.flappy;

import javax.swing.*;
import java.awt.*;

public class Renderer extends JPanel {

    private static final long serialVersionUID = 2L;

    private FlappyBirdGame flappyBirdGame;

    public Renderer(FlappyBirdGame flappyBirdGame) {
        this.flappyBirdGame = flappyBirdGame;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        repaint(g);
    }

    public void paintColumn(Graphics g, Rectangle column) {
        g.setColor(Color.green.darker());
        g.fillRect(column.x, column.y, column.width, column.height);
    }


    public void repaint(Graphics g) {
        g.setColor(Color.cyan);
        g.fillRect(0, 0, FlappyBirdGame.WIDTH, FlappyBirdGame.HEIGHT);

        g.setColor(Color.orange);
        g.fillRect(0, FlappyBirdGame.HEIGHT - 120, FlappyBirdGame.WIDTH, 120);

        g.setColor(Color.green);
        g.fillRect(0, FlappyBirdGame.HEIGHT - 120, FlappyBirdGame.WIDTH, 20);

        g.setColor(Color.red);
        final Rectangle bird = flappyBirdGame.getBird();
        g.fillRect(bird.x, bird.y, bird.width, bird.height);

        for (Rectangle column : flappyBirdGame.getColumns()) {
            paintColumn(g, column);
        }

        g.setColor(Color.white);
        g.setFont(new Font("Arial", 1, 100));

        if (!flappyBirdGame.isStarted()) {
            g.drawString("Click to start!", 75, FlappyBirdGame.HEIGHT / 2 - 50);
        }

        if (flappyBirdGame.isGameOver()) {
            g.drawString("Game Over!", 100, FlappyBirdGame.HEIGHT / 2 - 50);
        }

        if (!flappyBirdGame.isGameOver() && flappyBirdGame.isStarted()) {
            g.drawString(String.valueOf(flappyBirdGame.getScore()), FlappyBirdGame.WIDTH / 2 - 25, 100);
        }
    }
}
