package com.alex.flappy;

import java.awt.Rectangle;
import java.util.List;
import java.util.Random;

import static com.alex.flappy.FlappyBirdGame.HEIGHT;
import static com.alex.flappy.FlappyBirdGame.WIDTH;

public class GameController {

    private final FlappyBirdGame fpGame;

    private final Random rand;

    public GameController(FlappyBirdGame fpGame) {
        this.fpGame = fpGame;
        rand = new Random();
    }

    public void jump() {
        if (fpGame.isGameOver()) {

            fpGame.restart();

            addColumn(true);
            addColumn(true);
            addColumn(true);
            addColumn(true);
        }

        if (!fpGame.isStarted()) {
            fpGame.start();
        } else if (!fpGame.isGameOver()) {
            if (fpGame.getYMotion() > 0) {
                fpGame.setyMotion(0);
            }

            fpGame.incYMotion(-10);
        }
    }

    public void addColumn(boolean start) {
        int space = 300;
        int width = 100;
        int height = 50 + rand.nextInt(300);

        final List<Rectangle> columns = fpGame.getColumns();

        if (start) {
            columns.add(new Rectangle(WIDTH + width + columns.size() * 300, HEIGHT - height - 120, width, height));
            columns.add(new Rectangle(WIDTH + width + (columns.size() - 1) * 300, 0, width, HEIGHT - height - space));
        } else {
            columns.add(new Rectangle(columns.get(columns.size() - 1).x + 600, HEIGHT - height - 120, width, height));
            columns.add(new Rectangle(columns.get(columns.size() - 1).x, 0, width, HEIGHT - height - space));
        }
    }
}
