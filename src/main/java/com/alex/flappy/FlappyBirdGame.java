package com.alex.flappy;

import java.awt.Rectangle;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class FlappyBirdGame implements Serializable {

    public static final int WIDTH = 800, HEIGHT = 800;

    private Rectangle bird;

    private List<Rectangle> columns;

    private int yMotion, score;

    private boolean gameOver, started;

    public FlappyBirdGame() {
        bird = new Rectangle(WIDTH / 2 - 10, HEIGHT / 2 - 10, 20, 20);
        columns = new ArrayList<>();
        gameOver = false;
        started = false;
    }

    public Rectangle getBird() {
        return bird;
    }

    public List<Rectangle> getColumns() {
        return columns;
    }

    public int getYMotion() {
        return yMotion;
    }

    public void incYMotion(int incBy) {
        this.yMotion += incBy;
    }

    public void setyMotion(int yMotion) {
        this.yMotion = yMotion;
    }

    public boolean isGameOver() {
        return gameOver;
    }

    public void gameOver() {
        this.gameOver = true;
    }

    public void restart() {
        this.bird = new Rectangle(WIDTH / 2 - 10, HEIGHT / 2 - 10, 20, 20);
        columns.clear();
        yMotion = 0;
        score = 0;

        this.gameOver = false;
    }

    public boolean isStarted() {
        return started;
    }

    public void start() {
        this.started = true;
    }

    public void incScore() {
        score++;
    }

    public int getScore() {
        return score;
    }
}
