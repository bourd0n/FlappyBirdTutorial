package com.alex.flappy;

import javax.swing.*;

public class FlappyBirdStart {

    public static void main(String[] args) {
        FlappyBirdGame flappyBirdGame = new FlappyBirdGame();

        GameController gameController = new GameController(flappyBirdGame);

        JFrame jframe = new JFrame();
        final Renderer renderer = new Renderer(flappyBirdGame);
        jframe.add(renderer);

        Timer timer = new Timer(20, new TimerActionListener(flappyBirdGame, renderer, gameController));

        jframe.setTitle("Flappy Bird");
        jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jframe.setSize(FlappyBirdGame.WIDTH, FlappyBirdGame.HEIGHT);

        final MouseKeyListener mouseKeyListener = new MouseKeyListener(gameController);
        jframe.addMouseListener(mouseKeyListener);
        jframe.addKeyListener(mouseKeyListener);
        jframe.setResizable(false);
        jframe.setVisible(true);

        gameController.addColumn(true);
        gameController.addColumn(true);
        gameController.addColumn(true);
        gameController.addColumn(true);

        timer.start();
    }

}
